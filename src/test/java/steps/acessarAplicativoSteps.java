package steps;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import cucumber.api.java.pt.Dado;
import geral.*;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;

public class acessarAplicativoSteps
{
    protected ExtentTest extentTest;
    private DesiredCapabilities cap = new DesiredCapabilities();
    private String app = Paths.get(System.getProperty("user.dir").toString() , "\\app\\" + testConfiguration.apkName).toString();

    public acessarAplicativoSteps()
    {
        this.extentTest = geral.extentReport.test;
    }

    @Dado("^que acesso a tela de login do app$")
    public void dadoQueAcessoATelaDeLoginDoSistema()
    {
        try
        {
            geral.extentReport.SalvarLogSucesso("Dado que acesso a tela de login do app");
            cap.setCapability("deviceName", testConfiguration.deviceName);
            cap.setCapability("platformVersion", testConfiguration.platformVersion);
            cap.setCapability("browserName", testConfiguration.browserName);
            cap.setCapability("noReset", testConfiguration.noReset);
            //cap.setCapability("autoWebview", testConfiguration.autoWebview);
            cap.setCapability("app", app.trim());
            cap.setCapability("platformName", testConfiguration.platformName);
            //cap.setCapability("appActivity", testConfiguration.appActivity);
            //cap.setCapability("appWaitActivity", testConfiguration.appWaitActivity);
            seleniumActions.appiumdriver = new AndroidDriver<AndroidElement>(new URL(testConfiguration.url), cap);
            seleniumActions.wait = new WebDriverWait(seleniumActions.appiumdriver, 100);
            seleniumActions.driver = new EventFiringWebDriver(seleniumActions.appiumdriver);
            seleniumActions.action = new Actions(seleniumActions.driver);

            utils.Sleep(15000);

            geral.extentReport.SalvarLogScreenShotSucesso("tela de login exibida.");
            hooks.status = Status.PASS;
            hooks.ultimoScenario =  true;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
    }
}
