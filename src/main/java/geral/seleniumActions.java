package geral;

import java.util.List;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import enums.byElements;

public class seleniumActions
{
    private static By by;
    public static AppiumDriver<AndroidElement> appiumdriver;
    public static EventFiringWebDriver driver;
    public static Actions action;
    public static TouchAction act;
    public static WebDriverWait wait;

    //Methods Selenium
    public static boolean WaiElement(By by)
    {
        int count = 0;
        boolean displayed = false;

        while(!displayed)
        {
            if(driver.findElements(by).size() > 0) displayed = true;

            utils.Sleep(1000);

            count++;

            if(count > 10) break;
        }

        return displayed;
    }

    public static WebElement GetElement(byElements byElements, String toFind)
    {
        switch (byElements)
        {
            case ClassName : //ClassName
                by = By.className(toFind);
                break;

            case CssSelector: //CssSelector
                by = By.cssSelector(toFind);
                break;

            case Id: //Id
                by = By.id(toFind);
                break;

            case LinkText: //LinkText
                by = By.linkText(toFind);
                break;

            case Name: //Name
                by = By.name(toFind);
                break;

            case PartialLinkText: //PartialLinkText
                by = By.partialLinkText(toFind);
                break;

            case TagName: //TagName
                by = By.tagName(toFind);
                break;

            case XPath: //XPath
                by = By.xpath(toFind);
                break;
        }

        if(WaiElement(by)) return driver.findElement(by);
        else return driver.findElement(by);
    }

    public static List<WebElement> GetElements(byElements byElements, String toFind)
    {
        switch (byElements)
        {
            case ClassName : //ClassName
                by = By.className(toFind);
                break;

            case CssSelector: //CssSelector
                by = By.cssSelector(toFind);
                break;

            case Id: //Id
                by = By.id(toFind);
                break;

            case LinkText: //LinkText
                by = By.linkText(toFind);
                break;

            case Name: //Name
                by = By.name(toFind);
                break;

            case PartialLinkText: //PartialLinkText
                by = By.partialLinkText(toFind);
                break;

            case TagName: //TagName
                by = By.tagName(toFind);
                break;

            case XPath: //XPath
                by = By.xpath(toFind);
                break;
        }

        if(WaiElement(by)) return driver.findElements(by);
        else return null;
    }

    public static void Sendkeys(WebElement element, String text)
    {
        element.clear();
        element.sendKeys(text);
        extentReport.SalvarLogScreenShotSucesso("Escrevendo no campo " + element.toString());
    }

    public static void SendkeysValue(WebElement element, String text)
    {
        ClearValue(element);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].value = '" + text + "';", element);
        extentReport.SalvarLogScreenShotSucesso("Escrevendo no campo " + element.toString());
    }

    public static void Clear(WebElement element)
    {
        element.clear();
    }

    public static void ClearValue(WebElement element)
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].value = '';", element);
    }

    public static void Clicks(WebElement element)
    {
        element.click();
        extentReport.SalvarLogScreenShotSucesso("Clicando no campo " + element.toString());
    }

    public static void ClicksJS(WebElement element)
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", element);
        extentReport.SalvarLogScreenShotSucesso("Clicando no campo " + element.toString());
    }

    public static void ComboBoxSelect(WebElement element, String text)
    {
        Select selectElement = new Select(element);
        selectElement.selectByVisibleText(text);
        extentReport.SalvarLogScreenShotSucesso("Selecionando no campo " + element.toString());
    }

    public static String Text(WebElement element)
    {
        if(!element.getText().equals("")) return element.getText();
        else return "";
    }

    public static Boolean ValidacoesDeElementos(WebElement element)
    {
        if(element.isEnabled()) return true;
        else return false;
    }

    public static void Scrool(int x, int y)
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("scroll("+ x +"," + y + ");");
    }

    public static void SelecionaIFrame(WebElement element)
    {
        driver.switchTo().frame(element);
    }
}
