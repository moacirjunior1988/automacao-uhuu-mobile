package geral;

public class testConfiguration
{
    public static String apkName = utils.getProp("apkName");
    public static String url =  utils.getProp("url");
    public static String appActivity = utils.getProp("appActivity");
    public static String appWaitActivity = utils.getProp("appWaitActivity");
    public static String deviceName = utils.getProp("deviceName");
    public static String platformVersion = utils.getProp("platformVersion");
    public static String browserName = utils.getProp("browserName");
    public static String platformName = utils.getProp("platformName");
    public static String autoWebview = utils.getProp("autoWebview");
    public static String noReset = utils.getProp("noReset");
}
